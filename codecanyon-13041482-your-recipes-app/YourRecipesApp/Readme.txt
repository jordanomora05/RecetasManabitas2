Package Hierarchy :

AndroidStudioProjects      - Android Source Code for Android Studio
Documentation 		   - Help Guide for Installation and Configuration
EclipseProjects		   - Android Source Code for Eclipse
Migrate from Older Version - Migration guide from older version
Php Code Admin		   - Php Server Source Code for Admin Panel 

=========================================================================

Important Notice :
- Required Android Studio 2.2.2 or higher to import this android project
- Don�t open the Android Studio project in a directory that is too long path directory on windows, move the project to shorten path directory, as example : 
C:\AndroidStudioProjects\YourRecipesApp
- Eclipse doesn't support for Firebase, so, only can be integrated with OneSignal 

=========================================================================

Change Log :

Your Recipes App v2.4.0

- Build in latest version of Android Studio and Eclipse
- Update additional libraries to the latest version
- Firebase & OneSignal Push Notification
- Update listview to recyclerview
- Ripple effect on click listener added
- Easy way to enable or disable ads
- Easy way to switch in RTL mode (e.g Arabic Language)
- Improvement admin panel UI with material style

Your Recipes App v2.0.0

- Build in latest Android Studio and Eclipse
- Update additional libraries to the latest version
- Add compatibility on Android Marshmallow 6.0
- RTL Version included
- Fix Invalid Click in Some Recipes Menu
- Google Analytics is Available Now
- Add New Settings Menu in Android App
- Navigation Drawer with Header Image
- Swipe Down to Refresh Menu
- New Look in Recipes Detail View
- Migrate Push Notification from Parse to GCM
- Send Push Notification directly from Admin Panel

Your Recipes App v1.0.0

- Initial Release