package com.solodroid.yourrecipesapp;

public class Config {

    //your admin panel url here
    public static final String SERVER_URL = "http://dimasword.com/demo/recipes";

    //set true to enable ads or set false to disable ads
    public static final boolean ENABLE_ADMOB_ADS = true;

    //set true to enable exit dialog or set false to disable exit dialog
    public static final boolean ENABLE_EXIT_DIALOG = false;

    //set true if you want to enable RTL (Right To Left) mode, e.g : Arabic Language
    public static final boolean ENABLE_RTL_MODE = false;

    //number of grid column of recipes grid
    public static final int NUM_OF_COLUMNS = 2;

    //limit for number of recent recipes
    public static final int NUM_OF_RECENT_RECIPES = 50;

}
